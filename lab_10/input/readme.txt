Types of Format:
1. [message],[key],[seed]
	e.g.: "Besok_Jam_3, kencan, 32"
2. [message],[key]
	e.g.: "Love, live"

Important:
1. File must in .txt extension (Recommended)
2. File must encodes in UTF-8 (for large character)
