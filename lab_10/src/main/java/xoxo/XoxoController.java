package xoxo;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;

import xoxo.crypto.*;
import xoxo.exceptions.*;
import xoxo.key.*;
import xoxo.util.XoxoMessage;

/**
 * This class controls all the business
 * process and logic behind the program.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Nandhika Prayoga
 */
public class XoxoController {
     /**
     * Constant for default directory to output file
     */
    private static final String DEFAULT_OUTPUT_DIRECTORY_PATH = "../../../output/%s/%s";

    /**
     * Encrypted file number generator
     */
    private int encryptedFileNumberGenerator;
    
    /**
     * Decrypted file number generator
     */
    private int decryptedFileNumberGenerator;
    
    /**
     * The GUI object that can be used to get
     * and show the data from and to users.
     */
    private XoxoView gui;

    /**
     * Encryptor
     * process all particular message and then the output is encrypted file
     */
    private XoxoEncryption encryptor;
    
    /**
     * Decryptor
     * process all particular message and then the output is decrypted file
     */
    private XoxoDecryption decryptor;
    
    /**
     * Instance variable to make output file
     */
    private File file;
    
    /**
     * Writer to write a file
     * it can be encrypted message or decrypted message
     */
    private FileWriter writer;
    
    /**
     * Class constructor given the GUI object.
     * @param gui   The application to be runned
     */
    public XoxoController(XoxoView gui) {
        this.gui = gui;
        this.encryptedFileNumberGenerator = 1;
        this.decryptedFileNumberGenerator = 1;
    }

    /**
     * Main method that runs all the business process.
     */
    public void run() {
        this.gui.setEncryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String message = gui.getMessageText();
                String key = gui.getKeyText();
                encryptor = new XoxoEncryption(key);
                String seed = gui.getSeedText();
                XoxoMessage log; 
                try {
                    if (seed.equals("")) {
                        log = encryptor.encrypt(message); 
                    } else {
                        log = encryptor.encrypt(message, Integer.parseInt(seed));
                    }
                    String encryptedMessage = String.format("Encrypted Message: \"%s\", " +
                                                "Hug Key: \"%s\"",
                                                log.getEncryptedMessage(), log.getHugKey().getKeyString()); 
                    gui.appendLog(encryptedMessage);
                    String fileName = "";
                    boolean repeat;
                    do {
                        fileName = String.format("encrypted_message_%d.enc", encryptedFileNumberGenerator++);
                        File file = new File(String.format(DEFAULT_OUTPUT_DIRECTORY_PATH, 
                            "encryption files", fileName));
                        repeat = file.exists();
                    } while (repeat);
                    writeFile("encryption files", fileName, encryptedMessage);
                } catch (InvalidCharacterException | KeyTooLongException | 
                    RangeExceededException | SizeTooBigException | IOException exception) {
                    gui.showAlert(exception.getMessage());
                    return ;
                }
            }
        });

        this.gui.setDecryptFunction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String message = gui.getMessageText();
                String key = gui.getKeyText();
                decryptor = new XoxoDecryption(key);
                String seed = gui.getSeedText();
                String log;
                try {
                    if (seed.equals("")) {
                        log = decryptor.decrypt(message);
                    } else {
                        log = decryptor.decrypt(message, Integer.parseInt(seed));
                    }
                    String decryptedMessage = String.format("Decrypted Message: \"%s\"",
                                                log); 
                    gui.appendLog(decryptedMessage);
                    String fileName = "";
                    boolean repeat;
                    do {
                        fileName = String.format("decrypted_message_%d.txt", decryptedFileNumberGenerator++);
                        File file = new File(String.format(DEFAULT_OUTPUT_DIRECTORY_PATH, 
                            "encryption files", fileName));
                        repeat = file.exists();
                    } while (repeat);
                    writeFile("decryption files", fileName, decryptedMessage);
                } catch (InvalidCharacterException | KeyTooLongException | 
                    RangeExceededException | SizeTooBigException | IOException exception) {
                    gui.showAlert(exception.getMessage());
                    return ;
                }
            }   
        });
    }

    /**
     * Write file output
     * 
     * @param outputFolder  Name of folder destination
     * @param fileName      Name of file destination
     * @param log           Information will be stored
     */
    public void writeFile(String outputFolder, String fileName, String log) throws IOException {
        file = new File(String.format(DEFAULT_OUTPUT_DIRECTORY_PATH, outputFolder, fileName));
        file.createNewFile();
        writer = new FileWriter(file);    
        writer.write(log);
        writer.flush();
        writer.close();
    }

    /**
     * Write file output
     * 
     * @param outputFolder  name of folder destination
     * @param fileName      name of file destination
     * @param log           All information contains message itself and Hug Key
     */
    public void writeFile(String outputFolder, String fileName, XoxoMessage log) throws IOException {
        this.writeFile(outputFolder, fileName, log.getEncryptedMessage());
    } 
}