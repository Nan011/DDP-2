package xoxo;

import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JOptionPane;

/**
 * This class handles most of the GUI construction.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author Nandhika Prayoga
 */
public class XoxoView {
    /**
     * A frame that used to become application
     * represented GUI itself
     */
    private JFrame frame;
    
    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;

    
    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField; 

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    /**
     * A button that when it is clicked, it gets file from custom file.
     */
    private JButton customInputButton;

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        frame = new JFrame("Encryptor-Decryptor");
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(335, 465));
        frame.setResizable(true);

        JPanel panel = new JPanel(null);
        this.messageField = new JTextField();
        this.keyField = new JTextField();
        this.seedField = new JTextField();
        JLabel messageLabel = new JLabel("Message");
        JLabel keyLabel = new JLabel("Key");
        JLabel seedLabel = new JLabel("Seed");
        JLabel logLabel = new JLabel("Log");
        this.encryptButton = new JButton("Encrypt");
        this.decryptButton = new JButton("Decrypt");
        this.logField = new JTextArea();
        this.logField.setEditable(false);
        
        this.messageField.setSize(125, 100);
        this.keyField.setSize(125, 25);
        this.seedField.setSize(125, 25);
        messageLabel.setSize(125, 25);
        keyLabel.setSize(125, 25);
        seedLabel.setSize(125, 25);
        logLabel.setSize(125, 25);
        this.encryptButton.setSize(125, 30);
        this.decryptButton.setSize(125, 30);
        this.logField.setSize(275, 150);
        
        messageLabel.setLocation(25, 25);
        this.messageField.setLocation(25, 50);
        keyLabel.setLocation(175, 25);
        this.keyField.setLocation(175, 50);
        seedLabel.setLocation(175, 100);
        this.seedField.setLocation(175, 125);
        this.encryptButton.setLocation(25, 175);
        this.decryptButton.setLocation(175, 175);
        logLabel.setLocation(25, 230);
        this.logField.setLocation(25, 255);

        panel.add(messageLabel);
        panel.add(messageField);
        panel.add(keyLabel);
        panel.add(keyField);
        panel.add(seedLabel);
        panel.add(seedField);
        panel.add(encryptButton);
        panel.add(decryptButton);
        panel.add(logLabel);
        panel.add(logField);

        frame.add(panel);
        frame.pack();        
    }

    /**
     * Gets the message from the message field.
     * 
     * @return The input message string.
     */
    public String getMessageText() {
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     * 
     * @return The input key string.
     */
    public String getKeyText() {
        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     * 
     * @return The input key string.
     */
    public String getSeedText() {
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    public void showAlert(String message) {
        JOptionPane.showMessageDialog(frame, message);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */

    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }
    
    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     * 
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}