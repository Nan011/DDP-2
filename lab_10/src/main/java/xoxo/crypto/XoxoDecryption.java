package xoxo.crypto;

import xoxo.key.HugKey;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 * 
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
    }

    /**
     * Decrypts an encrypted message.
     * 
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        String decryptedMessage = "";
        final int length = encryptedMessage.length();
        for (short index = 0; index < length; index++) {
            int k = this.hugKeyString.charAt(index % hugKeyString.length()) ^ seed;
            int k1 = k - 'a';
            decryptedMessage += (char) (encryptedMessage.charAt(index) ^ k1);
        }
        return decryptedMessage;
    }

    public String decrypt(String encryptedMessage) {
        return this.decrypt(encryptedMessage, HugKey.DEFAULT_SEED);
    }
}