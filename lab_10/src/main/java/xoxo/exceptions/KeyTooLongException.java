package xoxo.exceptions;

/**
 * Exception to handle invalid character for every key's character
 */
public class KeyTooLongExceptions extends RuntimeException {
	public KeyTooLongExceptions(String message) {
		super(message);
	}

}