import java.util.Scanner;

public class RabbitHouse {
    // Fungsi untuk cek palindrom
    public static boolean isPalindrom(String word) {
        if (word.length() <= 1) {
            return true;
        } else if (word.charAt(0)==word.charAt(word.length()-1)) {
            return isPalindrom(word.substring(1, word.length()-1));
        } else {
            return false;
        }
    }
    // Fungsi untuk menjalankan perhitungan normal
    public static int normal(int number) {
        if (number == 1) {
            return 1;
        } 
        else {
            return 1 + number * normal(number - 1);
        } 
    }
    // Fungsi untuk menghasilkan semua anak dari suatu induk kata
    public static int combination(String word, int index) {
        if (index == word.length()) {
            return 0;
        } else {
            String word1 = word.substring(0, index) + word.substring(index+1);
            return palindrom(word1) + combination(word, index + 1);
        }
    }
    // Fungsi untuk menjalankan perhitungan terhadap kasus kata yang palindrom
    public static int palindrom(String word) {
        if (word.length() == 1 || isPalindrom(word.toLowerCase())) {
            return 0;
        } else {
            return 1 + combination(word, 0);
        }
    }

    public static void main(String args[]) {
        // Deklarasi input dari class Scanner
        Scanner input = new Scanner(System.in);
        String mode, word;
        mode = input.next(); // mode normal atau palindrom
        word = input.next(); // kata yang akan dikalkulasikan
        if (mode.equals("normal")) {  // case untuk kalkulasi normal
            System.out.println(normal(word.length()));
        } else if (mode.equals("palindrom")) { // case untuk kalkulasi terhadap kasus kata palindrom
            System.out.println(palindrom(word));
        }   
    }
}