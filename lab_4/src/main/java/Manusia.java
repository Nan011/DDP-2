
class Manusia {
    // Membentuk class manusia
    private String nama;
    private byte umur;
    private long uang;
    private float kebahagiaan = (float) 50.0;
    private boolean telahMeninggal = false;
    private static Manusia manusiaTerakhir;
    // Constructor untuk dua parameter
    Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = (byte) umur;
        this.uang = 50000;
        manusiaTerakhir = this;
    }
    // Constructor untuk tiga parameter dengan modifikasi tambahan yaitu set uang di paramter ke-3
    Manusia(String nama, int umur, long uang) {
        this.nama = nama;
        this.umur = (byte) umur;
        this.uang = uang;
        manusiaTerakhir = this;
    }
    // Method untuk memunculkan nama manusia ini
    public String getNama() {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
            return "";
        } else {
            return this.nama;   
        }
    }
    // Method untuk memunculkan umur manusia ini
    public byte getUmur() {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
            return 0;
        } else {
            return this.umur;
        } 
    }
    // Method untuk memunculkan uang manusia ini
    public long getUang() {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
            return 0;
        } else {
            return this.uang;
        }
    }
    // Method untuk memunculkan kebahagiaan manusia ini
    public float getKebahagiaan() {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
            return 0;
        } else {
            return this.kebahagiaan;
        }
    }
    // Method untuk set uang manusia ini
    public void setUang(long uang) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else {
            this.uang = uang;
        }
        return ;
    } 
    // Method untuk set kebahagiaan manusia ini
    public void setKebahagiaan(double kebahagiaan) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else if (kebahagiaan > 100.0) {
            this.kebahagiaan = (float) 100.0;
        } else if (kebahagiaan < 0) {
            this.kebahagiaan = 0;
        } else {
            this.kebahagiaan = (float) kebahagiaan;
        }
        return ;
    }
    // Method overload dengan satu paramter ketika manusia ini ingin memberi uang yang tidak pasti
    public void beriUang(Manusia penerima) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else if (penerima.telahMeninggal) {
            System.out.println(penerima.nama + " telah tiada");
        } else {
            int jumlah = 0;
            for(int i=0; i<penerima.nama.length(); i++) {
                jumlah += penerima.nama.charAt(i);
            }
            jumlah *= 100;
            if (uang < jumlah) {
                System.out.println(this.nama + " ingin memberi uang kepada " +
                    penerima.nama + " namun tidak memiliki cukup uang :'(");
            } else {
                this.uang -= jumlah;
                penerima.uang += jumlah;
                this.setKebahagiaan(this.kebahagiaan + jumlah / 6000.0);
                penerima.setKebahagiaan(penerima.kebahagiaan + jumlah / 6000.0);
                System.out.println(this.nama + " memberi uang sebanyak " + 
                    jumlah + " kepada " + penerima.nama + ", lalu mereka berdua senang"); 
            }
            return ;
        }
    }
    // Method overload dengan tambahan parameter berupa jumlah uang ketika manusia ini ingin memberi uang
    public void beriUang(Manusia penerima, int jumlah) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else if (penerima.telahMeninggal) {
            System.out.println(penerima.nama + " telah tiada");   
        } else {
            if (uang < jumlah) {
                System.out.println(this.nama + " ingin memberi uang kepada " +
                    penerima.nama + " namun tidak memiliki cukup uang :'(");
            } else {
                this.uang -= jumlah;
                penerima.uang += jumlah;
                this.setKebahagiaan(this.kebahagiaan + jumlah / 6000.0);
                penerima.setKebahagiaan(penerima.kebahagiaan + jumlah / 6000.0);
                System.out.println(this.nama + " memberi uang sebanyak " + 
                    jumlah + " kepada " + penerima.nama + ", lalu mereka berdua senang"); 
            }
            return ;
        }
    }
    // Method untuk manusia ini ingin bekerja
    public void bekerja(int durasi, int bebanKerja) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else {
            if (this.umur < 19) {
                System.out.println(this.nama + " belum boleh bekerja karena masih dibawah umur D:");
                return ; 
            }
            int bebanKerjaTotal = durasi * bebanKerja;
            int pendapatan;
            if (bebanKerjaTotal > this.kebahagiaan) {
                bebanKerjaTotal = (int) (kebahagiaan / bebanKerja) * bebanKerja;
                pendapatan = bebanKerjaTotal * 10000;
                this.uang += pendapatan;
                this.setKebahagiaan(this.kebahagiaan - bebanKerjaTotal);
                System.out.println(this.nama + 
                    " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + 
                    pendapatan);  
            } else {
                pendapatan = bebanKerjaTotal * 10000;
                this.uang += pendapatan;
                this.setKebahagiaan(this.kebahagiaan - bebanKerjaTotal);
                System.out.println(this.nama + " bekerja full time, total pendapatan : " + pendapatan);
            } 
            return ;
        }
    }
    // Method untuk manusia ini ingin rekreasi
    public void rekreasi(String namaTempat) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else {
            int biaya = namaTempat.length() * 10000;
            if (this.uang < biaya) {
                System.out.println(this.nama + " tidak mempunyai cukup uang untuk berekreasi di " + 
                    namaTempat + " :(");
            } else {
                this.uang -= biaya;
                this.setKebahagiaan(this.kebahagiaan + namaTempat.length());
                System.out.println(this.nama + " berekreasi di " + namaTempat + ", " + 
                    this.nama + " senang :)");
            }
        }
    }
    // Method ketika manusia ini terkena penyakit
    public void sakit(String penyakit) {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else {
            this.setKebahagiaan(this.kebahagiaan - penyakit.length());
            System.out.println(this.nama + " terkena penyakit " + penyakit + " :O");    
        }
    }
    // Method untuk mendeskripsikan manusia ini
    public String toString() {
        return "Nama\t\t: " + this.nama + "\n" + 
            "Umur\t\t: " + this.umur + "\n" +
            "Uang\t\t: " + this.uang + "\n" +
            "Kebahagiaan\t: " + this.kebahagiaan;
    }
    // Method untuk ketika sang manusia tersebut ingin meninggal
    public void meninggal() {
        if (telahMeninggal) {
            System.out.println(this.nama + " telah tiada");
        } else {
            System.out.println(this.nama + " meninggal dengan tenang, kebahagiaan: " + this.kebahagiaan);
            this.telahMeninggal = true;
            if (manusiaTerakhir == this) {
                this.uang = 0;
                System.out.println("Semua harta " + this.nama + " hangus");
                return ;
            } else {
                Manusia penerima = manusiaTerakhir;
                penerima.uang += this.uang;
                this.uang = 0;
                System.out.println("Semua harta " + this.nama + " disumbangkan untuk " + penerima.nama);
            }
        }
        return ;
    }
}