class BingoCard {
    public static final int EMPTY_NUMBER = 0;  

    private String board[][] = new String[5][5];    // Store every number on card
    private short xMark[] = new short[25];          // Store every X has been marked before
    private byte sumOfX = EMPTY_NUMBER;             // Number of X has been marked before
    private byte index[][] = new byte[199][2];      // Store index of number on card
    private boolean isBingo = false;          
    private static boolean gameStatus = true;       // To end game immediately when it was false
    
    private boolean checkBingo(byte i, byte j) {    // Checking bingo on card 
        for (byte index=0; index<5; index++) {      // Horizontally
            if (this.board[i][index].equals("X ") && index == 4) {
                return true;
            } else if (!this.board[i][index].equals("X ")) {
                break;
            }
        }

        for (byte index=0; index<5; index++) {      // Vertically
            if (this.board[index][j].equals("X ") && index == 4) {
                return true;
            } else if (!this.board[index][j].equals("X ")) {
                break;
            }
        }

        if (i == j || i == 4 - j) { 
            for (byte index=0; index<5; index++) { // 1st diagonally
                if (this.board[index][index].equals("X ") && index == 4) {
                    return true;
                } else if (!this.board[index][index].equals("X ")) {
                    break;
                }
            }
            for (byte index=0; index<5; index++) { // 2nd diagonally
                if (this.board[index][4-index].equals("X ") && index == 4) {
                    return true;
                } else if (!this.board[index][4-index].equals("X ")) {
                    break;
                }
            }
        }
        return false;
    }

    public byte getX() {
        return this.sumOfX;
    }

    public boolean isBingo() {
        return this.isBingo;
    }

    public boolean getGameStatus() {
        return this.gameStatus;
    }

    public String markNum(int data) {   
        if (data < -99 || data > 99 || (this.index[data + 99][0]==0 && this.index[data + 99][0]==0)) {  // Number validation
            return "Kartu tidak memiliki angka " + data;
        }
        byte i = (byte) (this.index[data + 99][0] - 1);
        byte j = (byte) (this.index[data + 99][1] - 1);

        if (this.board[i][j].equals("X ")) {                
            return data + " sebelumnya sudah tersilang";
        } else {
            this.board[i][j] = "X ";
            this.xMark[this.sumOfX++] = (short) (data + 99); // Store data that has been marked with X
            String info = data + " tersilang";
            if (this.checkBingo(i, j)) {
                this.isBingo = true;
                this.gameStatus = false;
                info += "\nBINGO!\n" + this.info();
            }
            return info;
        }
    }

    public String restart() {
        for (byte i=0; i<this.sumOfX; i++) {
            this.board[this.index[this.xMark[i]][0]-1][this.index[this.xMark[i]][1]-1] = Integer.toString(this.xMark[i]-99); // Change X to number
        }
        this.sumOfX = 0;
        return "Mulligan!";
    }

    public void add(int i, int j, int data) {
        String dataString = Integer.toString(data);
        this.board[i][j] = dataString;              // Store the number
        this.index[data + 99][0] = (byte) (i + 1);  // Store i index 
        this.index[data + 99][1] = (byte) (j + 1);  // Store j index
        return ;
    }

    public String info() {
        String info = "";
        for (byte i=0; i<4; i++) {
            info += "| ";
            for (byte j=0; j<4; j++) {
                info += this.board[i][j] + " | ";
            }
            info += this.board[i][4] + " |\n";
        }
        return info + "| " + this.board[4][0] + " | " + this.board[4][1] + " | " +
            this.board[4][2] + " | " + this.board[4][3] + " | " + this.board[4][4] + " |";
    }
}
