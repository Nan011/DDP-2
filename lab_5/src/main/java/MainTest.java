import java.util.Scanner;	// Import Scanner to use Scanner class
public class MainTest {
	private static Scanner scan = new Scanner(System.in);
	private static BingoCard card = new BingoCard();
	public static void main(String[] args) {

		/* Making BingoCard */
		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				card.add(i, j, Integer.parseInt(scan.next()));
			}
			scan.nextLine();
		}

		/* Processing commands */
		while (card.getGameStatus()) {
			String[] input = scan.nextLine().toLowerCase().split(" ");
			if (input[0].equals("mark")) {
				System.out.println(card.markNum(Integer.parseInt(input[1])));
			} else if (input[0].equals("info")) {
				System.out.println(card.info());
			} else if (input[0].equals("restart")) {
				card.restart();
			} else {
				System.out.println("Incorrect command");
			}
		} 
	}
}