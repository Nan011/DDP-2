import java.util.Scanner;	// Import Scanner to use Scanner class
public class MainTestBonus {
	private static Scanner scan = new Scanner(System.in);
	private static String[] names;
	private static int n; 	// The number of players
	public static void main(String[] args) {
		n = Integer.parseInt(scan.next().trim()); // Input number
		BingoCardBonus[] cards = new BingoCardBonus[n];
		names = scan.nextLine().trim().split(" "); // Input name of players

		/* Making BingoCard for every player */
		for (int i=0; i<n; i++) {	
			cards[i] = new BingoCardBonus(names[i]);
			for (int j=0; j<5; j++) {
				for (int k=0; k<5; k++) {
					cards[i].add(j, k, Integer.parseInt(scan.next()));
				}
				scan.nextLine();
			}
		}

		/* Processing commands */
		while (cards[0].getGameStatus()) {
			String[] input = scan.nextLine().toLowerCase().split(" ");
			for (int i=0; i<n; i++) {
				if (input[0].equals("mark")) {
					System.out.println(cards[i].markNum(Integer.parseInt(input[1])));
				} else if (input[0].equals("info")) {
					System.out.println(cards[i].info());
				} else if (input[0].equals("restart")) {
					cards[i].restart();
				} else {
					System.out.println("Incorrect command");
				}
			}
		} 
	}
}