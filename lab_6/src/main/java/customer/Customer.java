package customer;

import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;
import theater.Theater;

public class Customer {
	private String name;
	private String gender;
	private int age;
	private ArrayList<Ticket> tickets;

	public Customer(String name, String gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.tickets = new ArrayList<Ticket>();
	}

	public String getName() {
		return this.name;
	}

	public Ticket orderTicket(Theater theater, String title, String day, String type) {
		boolean type0 = false;
		if (type.toLowerCase().equals("3 dimensi")) {
			type0 = true;
		} else if (type.toLowerCase().equals("biasa")) {
			type0 = false;
		}
		Ticket ticket = theater.sellTicket(this.name, this.age, new Ticket(new Movie(title), day, type0));

		if (ticket != null) {
			this.tickets.add(ticket);
		}
		return ticket;
	}

	public void watchMovie(Ticket ticket) {
		boolean alreadyWatch = false;
		for (int i=0; i<this.tickets.size(); i++) {
			if (this.tickets.get(i).equals(ticket)) {
				Theater.changeTicketStatus(this.tickets.get(i));
				alreadyWatch = true;
			}
		}
		
		if (alreadyWatch) {
			Theater.printWatched(this.name, ticket.getMovieName());
		}
	}

	public void cancelTicket(Theater theater) {
		int index = this.tickets.size()-1;
		if (index < 0) {
			return ;
		}

		theater.refundMoney(this.name, this.tickets);
		return ;
	}

	public void findMovie(Theater theater, String title) {
		theater.checkMovie(this.getName(), title);
		return ;
	}
}