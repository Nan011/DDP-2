package movie;
public class Movie {
	private String title;
	private String genre;
	private int duration;
	private String rating;
	private int ratingNumber;
	private String type;

	public Movie(String title) {
		this(title, "Undefined", 0, null, null);
	}

	public Movie(String title, String rating, int duration, String genre, String type) {
		this.title = title;
		this.genre = genre;
		this.duration = duration;
		this.rating = rating;
		this.ratingNumber = this.categorizeRating(this.rating);
		this.type = type;
	}

	private int categorizeRating(String rating) {
		if (rating.toLowerCase().equals("remaja")) {
			return 13;
		} else if (rating.toLowerCase().equals("dewasa")) {
			return 17;
		} else {
			return 0;
		}
	}

	public String getTitle() {
		return this.title;
	}

	public int getRatingNumber() {
		return this.ratingNumber;
	}

	public String getRating() {
		return this.rating;
	}

	public String toString() {
		String margin = "------------------------------------------------------------------";
		return margin + "\nJudul\t: " + this.title +
			"\nGenre\t: " + this.genre +
			"\nDurasi\t: " + this.duration + " menit" +
			"\nRating\t: " + this.rating +
			"\nJenis\t: Film " + this.type + 
			"\n" + margin;
	}

	public boolean equals(Movie movie) {
		if (this.getTitle().equals(movie.getTitle())) {
			return true;
		} else {
			return false;
		}
	}
}