package theater;

import java.util.ArrayList;
import movie.Movie;
import ticket.Ticket;

public class Theater {
	private String name;
	private int wealth;
	private ArrayList<Ticket> tickets;
	private Movie[] movies;
	private String moviesInfo;

	public Theater(String name, int wealth, ArrayList<Ticket> tickets, Movie[] movies) {
		this.name = name;
		this.wealth = wealth;
		this.tickets = tickets;
		this.movies = movies;
		this.moviesInfo = "";
		this.createMovieInfo();
	}

	private void createMovieInfo() {
		for (Movie film: this.movies) {
			this.moviesInfo += film.getTitle() + ", ";
		}

		this.moviesInfo = this.moviesInfo.substring(0, this.moviesInfo.length()-2);
		return ;
	}

	public String getName() {
		return this.name;
	}

	public int getWealth() {
		return this.wealth;
	}

	public boolean checkMovie(String customerName, String title) {
		for (Movie movie: movies) {
			if (movie.getTitle().equals(title)) {
				 System.out.println(movie);
				 return true;
			}
		}

		System.out.println("Film " + title + 
			" yang dicari " + customerName + 
			" tidak ada di bioskop " + this.getName()
		);
		
		return false;
	}

	public Ticket sellTicket(String customerName, int customerAge, Ticket customerTicket) {
		for (int i=0; i<tickets.size(); i++) {
			Ticket theaterTicket = tickets.get(i);
			
			if (theaterTicket.equals(customerTicket) && theaterTicket.getRatingNumber() <= customerAge) {
				this.wealth += theaterTicket.getPrice();
				this.tickets.remove(i);
				System.out.println(customerName + " telah membeli tiket " + theaterTicket.getMovieName() + 
					" jenis " + theaterTicket.getType() + 
					" di " + this.name +
					" pada hari " + theaterTicket.getDay() +
					" seharga Rp. " + theaterTicket.getPrice()

				);
				
				return theaterTicket;
			} else if (theaterTicket.equals(customerTicket) && theaterTicket.getRatingNumber() > customerAge) {
				System.out.println(customerName + 
					" masih belum cukup umur untuk menonton " + theaterTicket.getMovieName() + 
					" dengan rating " + theaterTicket.getRating()
				);
				
				return null;
			}
		}
		System.out.println("Tiket untuk film " + customerTicket.getMovieName() + " jenis " + customerTicket.getType() + " dengan jadwal " + 
			customerTicket.getDay() + " tidak tersedia di " + this.getName()
		);
		return null;
	}

	public void refundMoney(String customerName, ArrayList<Ticket> customerTickets) {
		int index = customerTickets.size()-1;
		Ticket customerTicket = customerTickets.get(index);
		Movie customerMovie = customerTicket.getMovie();
		int ticketPrice = customerTicket.getPrice();
		boolean customerWatchStatus = customerTicket.getWatchStatus();

		if (!customerWatchStatus) {
			for (Movie movie: this.movies) {
				if (movie.equals(customerMovie) && this.wealth >= ticketPrice) {
					this.wealth -= ticketPrice;
					customerTickets.remove(index);
					System.out.println("Tiket film " + customerTicket.getMovieName() + 
						" dengan waktu tayang " + customerTicket.getDay() + 
						" jenis " + customerTicket.getType() + 
						" dikembalikan ke bioskop " + this.name
						);
					return ;
				} else if (movie.equals(customerMovie)) {
					System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop " + this.name + " lagi tekor...");
					return ;
				}
			}

			System.out.println("Maaf tiket tidak bisa dikembalikan, " + customerTicket.getMovieName() + 
				" tidak tersedia dalam " + this.name
			);
		} else {
			System.out.println("Tiket tidak bisa dikembalikan karena film " + 
				customerTicket.getMovieName() + " sudah ditonton oleh " + customerName);
			return ;
		}
		return ;
	}

	public void setWatchStatus(Ticket ticket) {
		ticket.setWatchStatus(true);
		return ;
	}

	public void printInfo() {
		String margin = "------------------------------------------------------------------";
		System.out.println(margin + "\nBioskop\t\t\t: " + this.getName() +
			"\nSaldo Kas\t\t: " + this.wealth +
			"\nJumlah Tiket tersedia\t: " + this.tickets.size() +
			"\nDalam Film Tersedia\t: " + this.moviesInfo +
			"\n" + margin);
		return ;
	}

	public static void printTotalRevenueEarned(Theater[] theaters) {
		String margin = "------------------------------------------------------------------";
		String info = "";
		int wealthTotal = 0;
		
		for (Theater theater: theaters) {
			info += "\nBioskop\t\t: " + theater.getName() +
				"\nSaldo Kas\t: " + theater.getWealth() +
				"\n";
			wealthTotal += theater.getWealth();
		}

		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + wealthTotal + 
			"\n" + margin + info + margin);
		return ;
	}

	public static void printWatched(String customerName, String movieName) {
		System.out.println(customerName + " telah menonton film " + movieName);
	}

	public static void changeTicketStatus(Ticket ticket) {
		ticket.setWatchStatus(true);
		return ;
	}
}