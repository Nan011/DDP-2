package ticket;
import movie.Movie;

public class Ticket {
	private Movie movie;
	private String day;
	private int price;
	private String type;
	private boolean watchStatus;

	public Ticket(Movie movie, String day, boolean type) {
		this.movie = movie;
		this.day = day;
		this.type = (type)? "3 Dimensi": "Biasa";
		this.price = categorizeTicketPrice(this.day);
		this.watchStatus = false;
	}

	private int categorizeTicketPrice(String day) {
		int price = 0;
		if (day.equals("Sabtu") || day.equals("Minggu")) {
			price += 100000;
		} else {
			price += 60000;
		}

		if (this.type.equals("3 Dimensi")) {
			price += price / 5;
		}

		return price;
	}

	public Movie getMovie() {
		return this.movie;
	}

	public String getMovieName() {
		return this.movie.getTitle();
	}

	public String getDay() {
		return this.day.substring(0, 1).toUpperCase() + this.day.substring(1);
	}

	public int getPrice() {
		return this.price;
	}

	public String getType() {
		return this.type;
	}

	public String getRating() {
		return this.movie.getRating();
	}

	public int getRatingNumber() {
		return this.movie.getRatingNumber();
	}

	public boolean getWatchStatus() {
		return this.watchStatus;
	}

	public void setWatchStatus(boolean status) {
		this.watchStatus = status;
	}

	public String toString() {
		String margin = "\n------------------------------------------------------------------";
		return margin + "\nFilm\t:" + this.movie.getTitle() +
			"\nJadwal Tayang\t:" + this.getDay() +
			"\nJenis\t:" + this.type +
			margin;
	}

	public boolean equals(Ticket ticket) {
		if (this.getMovieName().equals(ticket.getMovieName()) && 
			this.getDay().equals(ticket.getDay()) &&
			this.getType().equals(ticket.getType())) {
			return true;
		}
		return false;
	} 
}