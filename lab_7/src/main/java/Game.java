import character.*;
import java.util.ArrayList;

public class Game{
    private ArrayList<Player> players = new ArrayList<Player>();
    private ArrayList<Player> eater = new ArrayList<Player>();
    
    /**
     * Fungsi untuk mencari karakter dengan nilai kembalian berupa objek Player
     * @param String name nama karakter yang ingin dicari
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player player: this.players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }
    /**
     * fungsi untuk mencari karakter dengan nilai kembalian berupa index ke-n, n >= 0
     * @param String name nama karakter yang ingin dicari
     * @return int: return index ke-n, jika tidak ada return -1 
     */
    public int find(Player player) {
        for (int i = 0; i < this.players.size(); i++) {
            if (this.players.get(i).equals(player)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        if (isPlayerExist(chara)) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            switch(tipe) {
                case "Monster": {
                    players.add(new Monster(chara, hp));
                    break;
                } case "Magician": {
                    players.add(new Magician(chara, hp));
                    break;
                } case "Human": {
                    players.add(new Human(chara, hp));
                    break;
                }
            }
            return chara + " ditambah ke game";
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * @param String chara nama karakter yang ingin ditambahkan
     * @param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * @param int hp hp dari karakter yang ingin ditambahkan
     * @param String roar teriakan dari karakter
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        if (isPlayerExist(chara)) {
            return "Sudah ada karakter bernama " + chara;
        } else {
            if (tipe.equals("Monster")) {
                players.add(new Monster(chara, hp, roar));
                return chara + " ditambah ke game";
            } else {
                return "Tidak dapat menambahkan tipe karakter " + tipe;
            }
        } 
    }

    public int checkEater(Player player) {
        for (int i = 0; i < this.eater.size(); i++) {
            if (this.eater.get(i).equals(player)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * fungsi untuk menghapus character dari game
     * @param String chara character yang ingin dihapus
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara){
        if (isPlayerExist(chara)) {
            Player player = this.find(chara);
            int index = this.find(player);
            players.remove(index);
            return chara + " dihapus dari game";
        } else { 
            return "Tidak ada " + chara;
        }
    }


    /**
     * fungsi untuk menampilkan status character dari game
     * @param String chara character yang ingin ditampilkan statusnya
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        if (isPlayerExist(chara)) {
            Player player = this.find(chara);
            return player.info();
        } else {
            return "Tidak ada pemain";
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        String info = "";
        for (Player player: this.players) {
            if (player == null) {
                continue;
            } else {
                info += player.info() + "\n";
            }
        }
        return info;        
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * @param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        if (isPlayerExist(chara)) {
            Player player = this.find(chara);
            return player.diet();
        } else {
            return "Tidak ada " + chara;
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet(){
        String info = "";
        for (Player player: this.players) {
            if (player == null) {
                continue;
            } else {
                info += player.diet() + "\n";
            }
        }
        return info;   
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di serang
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        if (arePlayersExist(meName, enemyName)) {
            Player player1 = this.find(meName);
            Player player2 = this.find(enemyName);
            if (player1.canBeEaten()) {
                return Game.cancelAction(meName, enemyName, "attack");
            } else {
                player1.attack(player2);
                return "Nyawa " + enemyName + " kini " + player2.getHp();
            }
        } else {
            return "";
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di bakar
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        if (arePlayersExist(meName, enemyName)) {
            Player player1 = this.find(meName);
            Player player2 = this.find(enemyName);
            if (!(player1 instanceof Magician)) {
                return meName + " bukan seorang Magician";
            } else if (player1.canBeEaten()) {
                return Game.cancelAction(meName, enemyName, "burn");
            } else {
                ((Magician) player1).burn(player2);
                String info = "Nyawa " + enemyName + " " + player2.getHp();
                if (player2.isDead()) {
                    info += "\n dan matang";
                }
                return info;
            }
        } else {
            return "";
        }
    }

     /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * @param String meName nama dari character yang sedang dimainkan
     * @param String enemyName nama dari character yang akan di makan
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        if (arePlayersExist(meName, enemyName)) {
            Player player1 = this.find(meName);
            Player player2 = this.find(enemyName);
            if (player1.canBeEaten()) {
                return Game.cancelAction(meName, enemyName, "eat");
            } else {
                if (player1.canEat(player2) && (player1 instanceof Human && !(player2 instanceof Human) || 
                    player1 instanceof Monster)) {
                    player1.eat(player2);
                    if (checkEater(player1) == -1) {
                        System.out.println(player1.getName());
                        eater.add(player1);
                    }
                    int eaterIndex = checkEater(player2);
                    if (eaterIndex != -1) {
                        System.out.println(player2.getName());
                        eater.remove(eaterIndex);
                    }
                    players.remove(this.find(player2));
                    return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + player1.getHp();
                } else {
                    return meName + " tidak bisa memakan " + enemyName;
                }
            }   
        } else {
            return "";
        }
    }

     /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * @param String meName nama dari character yang akan berteriak
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        if (isPlayerExist(meName)) {
            Player player1 = this.find(meName);
            if (player1.status().equals("Monster")) {
                return ((Monster) player1).roar();
            } else {
                return meName + " tidak bisa berteriak";   
            }
        } else {
            return "Tidak ada " + meName;
        }
    }

     /**
     * fungsi untuk membatalkan aksi yang dilakukan player yang telah meninggal
     * @param String: nama player pertama (si Pelaku)
     * @param String: nama player kedua (si Target)
     * @param String: aksi yang ingin dibatalkan
     * @return String: return informasi pembatalan
     */
    public static String cancelAction(String player1Name, String player2Name, String action) {
        String info = player1Name + " tidak bisa";
        if (action.equals("eat")) {
            info += " memakan ";
        } else if (action.equals("attack")) {
            info += " menyerang ";
        } else if (action.equals("burn")) {
            info += " membakar ";
        }
        return info + player2Name;
    }

     /**
     * fungsi untuk mengecek keberadaan suatu player di dalam game
     * @param String: nama player
     * @return boolean: return true jika ada, false jika tidak ada
     */
    private boolean isPlayerExist(String playerName) {
        if (this.find(playerName) == null) {
            return false;
        } else {
            return true;
        }
    }

     /**
     * fungsi untuk mengecek keberadaan dua player di dalam game
     * @param String: nama player pertama
     * @param String: nama player kedua
     * @return boolean: return true jika ada salah satu atau keduanya, false jika tidak ada sama sekali
     */
    private boolean arePlayersExist(String player1Name, String player2Name) {
        if (isPlayerExist(player1Name) && isPlayerExist(player2Name)) {
            return true;
        } else if (isPlayerExist(player1Name)) {
            System.out.print("Tidak ada " + player1Name);
            return true;
        } else if (isPlayerExist(player2Name)) {
            System.out.print("Tidak ada " + player2Name);
            return true;
        } else {
            System.out.print("Tidak ada " + player1Name + " atau " + player2Name);
            return false;
        }
    }

    /**
     * fungsi untuk mencetak pemakan dan yang dimakan dalam visualisasi gambar horizontal
     * @return void: tidak ada yang di-return
     */
    public void cetakMenu() {
        for (Player player: this.eater) {
            System.out.print("(" + player.status() + " " + player.getName() + ") -----> (");
            Game.cetakMenu(player);
            System.out.print(")");
            System.out.println();
        }
    }

    /**
     * fungsi recursive untuk mencetak pemakan dan yang dimakan dalam visualisasi gambar horizontal
     * @param Player: objek Player yang digunakan untuk mencetak yang ia makan
     * @return void: tidak ada yang di-return
     */
    public static void cetakMenu(Player player) {
        ArrayList<Player> player1 = player.toBeEaten();
        for (int i = 0; i < player1.size(); i++) {
            Player player2 = player1.get(i);

            if (i > 0) {
                System.out.print(", ");
            }

            if (player2.toBeEaten().size() == 0) {
                System.out.print(player2.status() + " " + player2.getName());
            } else {
                System.out.print("(" + player2.status() + " " + player2.getName() + ") -----> (");
                Game.cetakMenu(player2);
                System.out.print(")");
            }

        }
    }
}