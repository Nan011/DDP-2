package character;

public abstract class Character {
	protected String name;
	protected int hp;

	public abstract String getName();
	public abstract int getHp();
	public abstract void eat(Player player);
	public abstract void attack(Player player);
	public abstract String diet();
	public abstract String status();

}