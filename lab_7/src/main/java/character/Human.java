package character;

public class Human extends Player {
	public Human(String name, int hp) {
		super(name, "Human", hp);
	}

	public Human(String name, String type, int hp) {
		super(name, type, hp);
	}	
}