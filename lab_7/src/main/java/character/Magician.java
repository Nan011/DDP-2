package character;

public class Magician extends Human {
	public Magician (String name, int hp) {
		super(name, "Magician", hp);
	}

	public void burn(Player player) {
		super.attack(player);
		super.canBeEaten();
	}
}