package character;

public class Monster extends Player {
	private String roarSound;
	public Monster (String name, int hp) {
		this(name, hp, "AAAAAAaaaAAAAAaaaAAAAAA");
	}

	public Monster(String name, int hp, String roarSound) {
		super(name, "Monster", 2 * hp);
		this.roarSound = roarSound;
	}

	public String roar() {
		return this.roarSound;
	}
}