package character;
import java.util.ArrayList;

public class Player extends Character {
	final public static int DEAD_HP = 0;
	final public static String INIT_DIET = "Belum ada yang termakan";

	protected String name;
	protected int hp;
	protected String role;
	protected boolean canBeEatenStatus;
	protected String deadInfo;
	protected ArrayList<Player> whoIsEaten;

	public Player(String name, String role, int hp) {
		this.name = name;
		this.role = role;
		this.hp = hp;
		this.canBeEatenStatus = false;
		this.deadInfo = "Masih Hidup";
		this.whoIsEaten = new ArrayList<Player>();
		this.isDead();	
	}

	public boolean isDead() {
		if (this.canBeEatenStatus) {
			return true;
		} else if (this.hp <= DEAD_HP) {
			this.hp = DEAD_HP;
			this.deadInfo = "Sudah meninggal dunia dengan damai";
			return true;
		} else {
			return false;
		}
	}

	public String getName() {
		return this.name;
	}

	public int getHp() {
		return this.hp;
	}

	public String status() {
		return this.role;
	}

	public String diet() {
		if (this.whoIsEaten.size() == 0) {
			return INIT_DIET;
		} else {
			String dietInfo = "";
			for (int i = 0; i < this.whoIsEaten.size(); i++) {
				Player player = this.whoIsEaten.get(i);
				if (i > 0) {
					dietInfo += ", ";
				}
				dietInfo += player.status() + " " + player.getName();
			}
			return dietInfo;
		}
	}

	public ArrayList<Player> toBeEaten() {
		return this.whoIsEaten;
	}

	private void changeHealth(int range) {
		this.hp += range;
		this.isDead();
	}

	public boolean canBeEaten() {
		if (this.hp == 0) {
			this.canBeEatenStatus = true;
		}
		return this.canBeEatenStatus;
	}

	public boolean canEat(Player player) {
		return player.canBeEaten();
	}

	public void eat(Player player) {
		this.hp += 15;
		this.whoIsEaten.add(player);
	}

	public void attack(Player player) {
		if (player instanceof Magician) {
			player.changeHealth(-20);
		} else {
			player.changeHealth(-10);
		}
	}

	public String info() {
		String fullDietInfo;
		if (this.whoIsEaten.size() == 0) {
			fullDietInfo = INIT_DIET;
		} else {
			fullDietInfo = "Memakan " + this.diet();
		}
		return this.role + " " + this.name + "\n" +
			"HP: " + this.hp + "\n" +
			this.deadInfo + "\n" +
			fullDietInfo;
	}
}
