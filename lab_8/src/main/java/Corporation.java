import java.util.HashMap;
import java.util.Map;
import employee.*;

public class Corporation {
	private String corporationName;
	private String chiefName;
	private HashMap<String, Employee> employees = new HashMap<String, Employee>();
	private HashMap<String, Integer> employeeIncomes = new HashMap<String, Integer>();
	private int numberOfEmployee;
	
	public Corporation(String corporationName, String chiefName) {
		this.corporationName = corporationName;
		this.chiefName = chiefName;
		this.numberOfEmployee = 0;
	}

	public boolean isThereEmployee(String name) {
		if (this.employees.get(name) == null) {
			return false;
		} else {
			return true;
		}
	}

	public String addEmployee(String name, String job, int income) {
		if (this.isThereEmployee(name)) {
			return String.format("Karyawan dengan nama %s telah terdaftar", name);
		} else {
			if (this.numberOfEmployee > 10000) {
				return String.format("Perusahaan %s tidak dapat mengrekrut karyawan lagi", this.corporationName);
			} else {
				if (job.equals("MANAGER")) {
					this.employees.put(name, new Manager(name, 10));
				} else if (job.equals("STAFF")) {
					this.employees.put(name, new Staff(name, 10));
				} else if (job.equals("INTERN")) {
					this.employees.put(name, new Intern(name, 0));
				}
				this.employeeIncomes.put(name, income);
				this.numberOfEmployee++;
				return String.format("%s mulai bekerja sebagai %s di %s", name, job, this.corporationName);
			}
		}
	}

	public String checkStatus(String name) {
		if (this.isThereEmployee(name)) {
			return String.format("%s %d", employees.get(name).getName(), employeeIncomes.get(name));
		} else {
			return "Karyawan tidak ditemukan";
		}
	}

	public String addWorker(String employeeName, String workerName) {
		if (this.isThereEmployee(employeeName) && this.isThereEmployee(workerName)) {
			Employee employee = this.employees.get(employeeName);
			Employee worker = this.employees.get(workerName);
			if (employee.getClass().isAssignableFrom(worker.getClass()) && 
				!employee.getClass().getName().equals(worker.getClass().getName())) {
				employee.addWorker(worker);
				return String.format("Karyawan %s telah menjadi bawahan %s", workerName, employeeName);
			} else {
				return "Anda tidak layak memiliki bawahan";
			}
		} else {
			return "Nama tidak berhasil ditemukan";
		}
	}

	public String payday() {
		String info = "";
		for (Map.Entry<String, Employee> entry: employees.entrySet()) {
			String employeeName = entry.getKey();
			Employee employee = this.employees.get(employeeName);
			Integer employeeIncome = this.employeeIncomes.get(employeeName);
			employee.isPaid(employeeIncome);
			if (employee.getPaydayOrder() % 6 == 0) {
				Integer newEmployeeIncome = (int) (employeeIncome * 1.1);
				this.employeeIncomes.put(employeeName, newEmployeeIncome);
				info += String.format("%s mengalami kenaikan gaji sebesar 10%% dari %d menjadi %d%n", 
					employeeName, employeeIncome, newEmployeeIncome
				);
			} 
		}
		info += "Semua karyawan telah diberikan gaji";
		return info;
	}
}