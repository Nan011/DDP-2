import java.util.Scanner;

class Lab8 {
	private static Scanner scan = new Scanner(System.in);
	private static String[] inputs;
	private static Corporation corporation1 = new Corporation("PT. TAMPAN", "AFFAN");
    public static void main(String[] args) {
    	 while (true) {
    	 	inputs = scan.nextLine().split(" ");
    	 	if (inputs[0].equals("TAMBAH_KARYAWAN")) {
    	 		System.out.println(corporation1.addEmployee(inputs[1], inputs[2], Integer.parseInt(inputs[3])));
    	 	} else if (inputs[0].equals("STATUS")) {
    	 		System.out.println(corporation1.checkStatus(inputs[1]));
    	 	} else if (inputs[0].equals("TAMBAH_BAWAHAN")) {
    	 		System.out.println(corporation1.addWorker(inputs[1], inputs[2]));
    	 	} else if (inputs[0].equals("GAJIAN")) {
    	 		System.out.println(corporation1.payday());
    	 	} else if (inputs[0].equals("exit")) {
    	 		break;
    	 	}
    	 }
    }
}