package employee;

public abstract class Employee {
	protected String name;
	protected int wealth;
	protected int workerOrder;
	protected int paydayOrder;
	protected Employee[] workers;

	public Employee(String name, int workersLimitation) {
		this.name = name;
		this.wealth = 0;
		this.workerOrder = 0;
		this.paydayOrder = 0;
		if (workersLimitation == 0) {
			this.workers = null;
		} else {
			this.workers = new Employee[workersLimitation];
		}
	}

	public void isPaid(int income) {
		this.paydayOrder++;
		this.wealth += income;
		return ;
	}

	public int getPaydayOrder() {
		return this.paydayOrder;
	}

	public void addWorker(Employee worker) {
		try {
			this.workers[this.workerOrder++] = worker;
		} catch (IndexOutOfBoundsException e) {
			return ;
		}
	}

	public String getName() {
		return this.name;
	}
}