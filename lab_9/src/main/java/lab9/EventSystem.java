package lab9;

import java.util.Map;
import java.util.HashMap;
import java.util.GregorianCalendar;
import java.math.BigInteger;

import lab9.user.User;
import lab9.event.Event;


/**
* Class representing event managing system
*
* @author Muhammad Zaky Khairuddin
* @author Nandhika Prayoga
*/
public class EventSystem {
    /**
    * List of events
    */
    private HashMap<String, Event> events;
    
    /**
    * List of users
    */
    private HashMap<String, User> users;
    
    /**
    * Constructor. Initializes events and users with empty lists.
    */
    public EventSystem()
    {
        this.events = new HashMap<String, Event>();
        this.users = new HashMap<String, User>();
    }

    /**
    * Checker method to check does user exist or not in this system 
    *
    * @param name   searched user's name
    * @return       true if exist, otherwise is false
    */
    private boolean doesUserExist(String name) {
        if (users.get(name) == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * Checker method to check does event exist or not in this system 
    *
    * @param name   searched event's name
    * @return       true if exist, otherwise is false
    */
    private boolean doesEventExist(String name) {
        if (events.get(name) == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * Categorizing date time of the event
    * 
    * @param startTime  beginning of event's time
    * @param endTime    ending of event's time
    * @return           dual-dimension array in specific data; contains year, month, day, hour, minute, and second
    *                   and fist array is beginning of event's time and the other is ending of event's time
    */
    private int[][] categorizeTime(String startTime, String endTime) {
        int[][] time = new int[2][6];
        for (byte index0 = 0; index0 < 2; index0++) {
            String[] temp;
            if (index0 == 0) {
                temp = startTime.split("_");
            } else {
                temp = endTime.split("_");
            }
            String[][] temp1 = {temp[0].split("-"), temp[1].split(":")};
            for (byte index1 = 0; index1 < 3; index1++) {
                time[index0][index1] = Integer.parseInt(temp1[0][index1]);
                time[index0][3 + index1] = Integer.parseInt(temp1[1][index1]);
            }
        }
        return time;
    }

    /**
    * Check whether both time of event is valid or not
    * 
    * @param time0  all data of time at the beginning time
    * @param time1  all data of time at the endin time
    * @return       true if time is valid, otherwise is false
    */
    private boolean isTimeValid(int[] time0, int[] time1) {
        for (byte index = 0; index < 6; index++) {
            if (time0[index] > time1[index]) {
                return false;
            } else if (time0[index] < time1[index]) {
                return true;
            }
        }
        return true;
    }

    /**
    * Method to check one of time among both time
    *
    * @param time0  first limitation of time
    * @param time1  time to be checked
    * @param time2  last limitation of time 
    * @return       true if time is overlapping, otherwise is false
    */
    private boolean isTimeOverlapping(int[] time0, int[] time1, int[] time2) {
        if (this.isTimeValid(time0, time1) && this.isTimeValid(time1, time2) &&
            !time0.equals(time1)) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * Method to check selected event by user is overlapping time or not with user events
    *
    * @param userName   name of user who own events
    * @param eventName  event to be checked whether is overlapping or not
    * @return           true if time is overlapping, otherwise is false
    */
    private boolean isTimeOverlapping(String userName, String eventName) {
        Event event0 = events.get(eventName);
        for (Event event1: users.get(userName).getEvents()) {
            if (!this.isTimeOverlapping(event0.getDate()[0], event1.getDate()[0], event0.getDate()[1]) || 
                !this.isTimeOverlapping(event1.getDate()[0], event0.getDate()[0], event1.getDate()[1])) {
                return true;
            }
        }
        return false;
    }

    /**
    * Getter method to get event by certain name 
    *
    * @param name   specific event's name
    * @return       if event exist then true, otherwise is null
    */
    public Event getEvent(String name) {
        return events.get(name);
    }

    /**
    * Getter method to get user by certain name 
    *
    * @param name   specific user's name
    * @return       if user exist then true, otherwise is null
    */
    public User getUser(String name) {
        return users.get(name);
    }
    
    /**
    * Method to add event into the system
    * It will not add at all if name of the event that has been registered on system
    * Or do same thing if time is not valid
    * 
    * @param name               event's name which one want to be registered on system
    * @param startTimeStr       beginning of event's time
    * @param endTimeStr         ending of event's time
    * @param costPerHourSter    cost the event
    * @return                   information and it depends event can be registered or not
    */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        if (this.doesEventExist(name)) {
            return String.format("Event %s sudah ada!", name);
        } else {
            int[][] time = categorizeTime(startTimeStr.replaceAll(" ", ""), endTimeStr.replaceAll(" ", ""));
            if (this.isTimeValid(time[0], time[1])) {
                this.events.put(name, 
                                new Event(name, 
                                            new GregorianCalendar(time[0][0], time[0][1], time[0][2], 
                                                                    time[0][3], time[0][4], time[0][5]),
                                            new GregorianCalendar(time[1][0], time[1][1], time[1][2], 
                                                                    time[1][3], time[1][4], time[1][5]),
                                            new BigInteger(costPerHourStr.replaceAll(" ", ""))));

                return String.format("Event %s berhasil ditambahkan!", name);
            } else {
                return "Waktu yang diinputkan tidak valid!";
            }
        }
    }
    
    /**
    * Method to add user into the system
    * It will not add at all if name of the user that has been registered on system
    * 
    * @param name               user's name which one want to be registered on system
    * @return                   information and it depends event can be registered or not
    */
    public String addUser(String name) {
        if (this.doesUserExist(name)) {
            return String.format("User %s sudah ada!", name);  
        } else {
            this.users.put(name, new User(name));
            return String.format("User %s berhasil ditambahkan!", name);
        }
    }
    
    /**
    * Method to register user and his/her event
    * It will not register if user or event doesn't exist
    * Or do same thing if timeoverlapping exist between selected event and user're events
    * 
    * @param userName   user's name
    * @param eventName  selected event's name
    * @return           information and it depends can be registered or not
    */
    public String registerToEvent(String userName, String eventName) {
        boolean userExist = this.doesUserExist(userName);
        boolean eventExist = this.doesEventExist(eventName);
        if (userExist && eventExist) {
            if (this.isTimeOverlapping(userName, eventName)) {
                return String.format("%s sibuk sehingga tidak dapat menghadiri %s", userName, eventName);
            } else {
                this.users.get(userName).addEvent(this.events.get(eventName));
                return String.format("%s berencana menghadiri %s!", userName, eventName);
            } 
        } else if (userExist) {
            return String.format("Tidak ada acara dengan nama %s!", eventName);
        } else if (eventExist) {
            return String.format("Tidak ada pengguna dengan nama %s!", userName);
        } else {
            return String.format("Tidak ada pengguna dengan nama %s dan acara dengan nama %s!",
                                    userName, eventName);
        }
    }
}