package lab9.event;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.math.BigInteger;

/**
* A class representing an event and its properties
*
* @author Muhammad Zaky Khairuddin
* @author Nandhika Prayoga
*/
public class Event {
    /** 
    * Name of event 
    */
    private String name;
    /** 
    * Date time of event 
    */
    private Calendar startDate, endDate;
    /** 
    * Event cost
    */
    private BigInteger costPerHour;
    
    /**
    * Constructor of this class
    * 
    * @param name           name of the event
    * @param startDate      beginning time of the event
    * @param endDate        ending time of the event
    * @param costPerHour    cost of the event
    */
    public Event(String name, GregorianCalendar startDate,
                    GregorianCalendar endDate, BigInteger costPerHour) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.costPerHour = costPerHour;
    }
    
    /**
    * Accessor for name field. 
    * @return name of this event instance
    */
    public String getName() {
        return this.name;
    }

    /**
    * Method to get date time this event
    * 
    * @return Date time and is represented by dual-dimension array, 
    *           first array is start of event time and last one is end of event time
    */
    public int[][] getDate() {
        int year0 = startDate.get(Calendar.YEAR);
        int month0= startDate.get(Calendar.MONTH);
        int day0 = startDate.get(Calendar.DAY_OF_MONTH);
        int hour0 = startDate.get(Calendar.HOUR_OF_DAY);
        int minute0 = startDate.get(Calendar.MINUTE);
        int second0 = startDate.get(Calendar.SECOND);

        int year1 = endDate.get(Calendar.YEAR);
        int month1 = endDate.get(Calendar.MONTH);
        int day1 = endDate.get(Calendar.DAY_OF_MONTH);
        int hour1 = endDate.get(Calendar.HOUR_OF_DAY);
        int minute1 = endDate.get(Calendar.MINUTE);
        int second1 = endDate.get(Calendar.SECOND);
        return new int[][] {{year0, month0, day0, hour0, minute0, second0},
                            {year1, month1, day1, hour1, minute1, second1}};
    }

    /**
    * Getter method for cost this event
    *
    * @return Event cost in decimal number
    */
    public BigInteger getCost() {
        return this.costPerHour;
    }
    
    /**
    * Profile this event that contains name, start of event time, end of event time, and cost of the event
    *
    * @return The profile of the event
    */
    public String toString() {
        int[][] dateInt = this.getDate();
        String[][] dateStr = new String[2][6];
        for (int index0 = 0; index0 < 2; index0++) {
            for (int index1 = 0; index1 < 6; index1++) {
                dateStr[index0][index1] = (dateInt[index0][index1] < 10)? "0" + dateInt[index0][index1]: 
                                                                                String.valueOf(dateInt[index0][index1]);
            }
        }
        return String.format("%s%n" +
                            "Waktu mulai: %s-%s-%s, %s:%s:%s%n" +
                            "Waktu selesai: %s-%s-%s, %s:%s:%s%n" +
                            "Biaya kehadiran: %s", 
                            this.name, dateStr[0][2], dateStr[0][1], dateStr[0][0],
                            dateStr[0][3], dateStr[0][4], dateStr[0][5],
                            dateStr[1][2], dateStr[1][1], dateStr[1][0],
                            dateStr[1][3], dateStr[1][4], dateStr[1][5],
                            costPerHour.toString());
    }
}
