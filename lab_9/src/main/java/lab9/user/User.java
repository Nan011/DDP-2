package lab9.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.math.BigInteger;

import lab9.event.Event;

/**
* Class representing a user, willing to attend event(s)
*
* @author Muhammad Zaky Khairuddin
* @author Nandhika Prayoga
*/
public class User implements Cloneable {
    /** Name of user */
    private String name;
    
    /** List of events this user plans to attend */
    private HashMap<String, Event> events;
    
    /**
    * Constructor
    * Initializes a user object with given name and empty event list
    *
    * @param name name of this user
    */
    public User(String name)
    {
        this.name = name;
        this.events = new HashMap<>();
    }
    
    /**
    * Accessor for name field
    * @return name of this instance
    */
    public String getName() {
        return name;
    }

    /**
    * Getter method to get event by specific name
    *
    * @param name   name of the event which one to be choosen
    * @return       Selected event
    */
    public Event getEvent(String name) {
        return (Event) this.events.get(name);
    }

    /**
    * Getter method to get total cost for every event from user's event has been registered
    *
    * @return total cost
    */
    public BigInteger getTotalCost() {
        BigInteger totalCost = new BigInteger("0");
        for (Event event: this.getEvents()) {
            totalCost = totalCost.add(event.getCost());
        }
        return totalCost;
    }
    
    /**
    * Adds a new event to this user's planned events
    * 
    * @param newEvent   event
    */
    public void addEvent(Event newEvent)
    {
        this.events.put(newEvent.getName(), newEvent);
    }

    /**
    * Returns the list of events this user plans to attend,
    * Sorted by their starting time.
    * Note: The list returned from this method is a copy of the actual
    *       events field, to avoid mutation from external sources
    *
    * @return list of events this user plans to attend
    */
    public ArrayList<Event> getEvents() {
        return new ArrayList<Event>(this.events.values());
    }

    /**
    * Method to make object more invidually, or copy itself
    * 
    * @param newEvent   event
    * @return           new copied object
    */    
    public Object clone() throws CloneNotSupportedException {  
        return super.clone();  
    }  
}
