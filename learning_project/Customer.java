class Customer {
	private String name;
	private boolean gender;
	private int age;
	private Ticket ticket;
	private boolean watchStatus;
	
	public Customer(String name, boolean gender, int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.watchStatus = false;
	}

	public String getName() {
		return this.name;
	}

	public Ticket orderTicket(Theater theater, String title, String day, String type) {
		this.ticket = theater.sellTicket(this.name, title, day, type);
		return this.ticket;
	}

	public void watchMovie(Ticket ticket) {
		this.watchStatus = true;
		return ;
	}

	public void cancelTicket(Theater theater) {
		theater.refundMoney(this.name, this.ticket, this.watchStatus);
		return ;
	}

	public void findMovie(Theater theater, String title) {
		theater.checkMovie(this.getName(), title);
		return ;
	}
}