import java.util.Scanner;
import java.util.ArrayList;
class BingoCard {
    private String name;
    private String board[][] = new String[5][5];
    private short markX[] = new short[25];
    private byte sumOfX = 0;
    private byte index[][] = new byte[199][2];
    private boolean gameStatus = true;
    private boolean restartStatus = true;
    
    BingoCard(String name) {
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
    }

    private boolean isBingo(byte i, byte j) {
        for (byte index=0; index<5; index++) {
            if (this.board[i][index].equals("X") && index == 4) {
                return true;
            } else if (!this.board[i][index].equals("X")) {
                break;
            }
        }

        for (byte index=0; index<5; index++) {
            if (this.board[index][j].equals("X") && index == 4) {
                return true;
            } else if (!this.board[index][j].equals("X")) {
                break;
            }
        }

        if (i == j || i == 4 - j) {
            for (byte index=0; index<5; index++) {
                if (this.board[index][index].equals("X") && index == 4) {
                    return true;
                } else if (!this.board[index][index].equals("X")) {
                    break;
                }
            }
            for (byte index=0; index<5; index++) {
                if (this.board[index][4-index].equals("X") && index == 4) {
                    return true;
                } else if (!this.board[index][4-index].equals("X")) {
                    break;
                }
            }
        }
        return false;
    }

    public String getName() {
        return this.name;
    }

    public byte getMarkX() {
        return this.sumOfX;
    }

    public boolean getGameStatus() {
        return this.gameStatus;
    }

    public void markNum(int data) {
        byte i = this.index[data + 99][0];
        byte j = this.index[data + 99][1];
        if (i==0 && j==0) {
            System.out.println("Kartu " + this.name + " tidak memiliki angka " + data);
            return ;
        }

        i -= 1;
        j -= 1;

        if (this.board[i][j].equals("X")) {
            System.out.println(data + " sebelumnya sudah tersilang");
        } else {
            this.board[i][j] = "X";
            this.markX[this.sumOfX++] = (short) (data + 99);
            System.out.println("Kartu " + this.name + " dengan angka " + data + " telah tersilang");
            if (this.isBingo(i, j)) {
                System.out.println("BINGO!");
                this.gameStatus = false;
                this.info();
            }
        }
        return ;
    }

    public void restart() {
        if (this.restartStatus) {
            for (byte i=0; i<this.sumOfX; i++) {
                this.board[this.index[this.markX[i]][0]-1][this.index[this.markX[i]][1]-1] = Integer.toString(this.markX[i]-99);
            }
            this.sumOfX = 0;
            System.out.println("Mulligan!");
            this.restartStatus = false;

        } else {
            System.out.println("Tidak bisa restart lagi");
        }
        return ;
    }

    public void add(int i, int j, int data) {
        String dataString = Integer.toString(data);
        this.board[i][j] = dataString;
        this.index[data + 99][0] = (byte) (i + 1);
        this.index[data + 99][1] = (byte) (j + 1);
    }

    public void info() {
        String info = this.name + "\n";
        for (byte i=0; i<5; i++) {
            info += "| ";
            for (byte j=0; j<4; j++) {
                info += this.board[i][j] + " | ";
            }
            info += this.board[i][4] + " |\n";
        }
        System.out.print(info);
        return ;
    } 
}

public class LearningProject {
	public static void main(String args[]) {
        ArrayList<BingoCard> players = new ArrayList<BingoCard>();
        Scanner input = new Scanner(System.in);
        int n = Integer.parseInt(input.next().trim());
        String names[] = input.nextLine().trim().split(" ");
        for (byte i=0; i<n; i++) {
            players.add(new BingoCard(names[i]));
            for (byte j=0; j<5; j++) {
                for (byte k=0; k<5; k++) {
                    players.get(i).add(j, k, Integer.parseInt(input.next()));
                }
            }
        }
        while (true) {
            String word[] = input.nextLine().toLowerCase().trim().split(" ");
            if (word[0].equals("end")) {
               break;
            }
            for (int i=0; i<n; i++) {
                if (word[0].equals("info")) {
                    players.get(i).info();
                } else if (word[0].equals("mark")) {
                    players.get(i).markNum(Integer.parseInt(word[1]));
                } else if (word[0].equals("restart")) {
                    players.get(i).restart();
                } else { 
                    System.out.println("Incorrect command");
                }
            }
        }
	}
}
