class Movie {
	private String title;
	private String genre;
	private float duration;
	private String rating;
	private String type;

	public Movie(String title, String genre, double duration, String rating, String type) {
		this.title = title;
		this.genre = genre;
		this.duration = (float) duration;
		this.rating = rating;
		this.type = type;
	}

	public String getTitle() {
		return this.title;
	}

	public String toString() {
		String margin = "------------------------------------------------------------------";
		return margin + "\nJudul\t: " + this.title +
			"\nGenre\t: " + this.genre +
			"\nDurasi\t: " + this.duration +
			"\nRating\t: " + this.rating +
			"\nJenis\t: " + this.type + 
			"\n" + margin;
	}
}