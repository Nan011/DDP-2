import java.util.ArrayList;
class Theater {
	private String name;
	private float wealth;
	private ArrayList<Ticket> tickets;
	private Movie[] movies;
	private String moviesInfo;

	Theater(String name, int wealth, ArrayList<Ticket> tickets, Movie[] movies) {
		this.name = name;
		this.wealth = wealth;
		this.tickets = tickets;
		this.movies = movies;
		this.moviesInfo = "";
		this.createMovieInfo();
	}

	private void createMovieInfo() {
		for (Movie film: this.movies) {
			this.moviesInfo += film.getTitle() + ", ";
		}

		this.moviesInfo = this.moviesInfo.substring(0, this.moviesInfo.length()-2);
		return ;
	}

	public String getName() {
		return this.name;
	}

	public float getWealth() {
		return this.wealth;
	}

	public boolean checkMovie(String customerName, String title) {
		for (Movie movie: movies) {
			if (movie.getTitle().equals(title)) {
				 System.out.println(movie);
				 return true;
			}
		}
		System.out.println(title + " yang dicari " + customerName + 
			" tidak ada di bioskop " + this.getName());
		return false;
	}

	public Ticket sellTicket(String name, String title, String day, String type) {
		for (int i=0; i<tickets.size(); i++) {
			Ticket ticket = tickets.get(i);
			if (ticket.getMovieName().equals(title) && ticket.getDay().equals(day) && ticket.getType().equals(type)) {
				this.wealth += ticket.getPrice();
				this.tickets.remove(i);
				System.out.println(name + " telah membeli tiket " + title + " jenis " + 
					type + " seharga Rp. " + ticket.getPrice());
				return ticket;
			} 
		}

		System.out.println("Tiket untuk film " + title + " jenis " + type + " pada hari " + 
			day + " tidak tersedia di " + this.getName());
		return null;
	}

	public void refundMoney(String customerName, Ticket customerTicket, boolean customerWatchStatus) {
		if (!customerWatchStatus) {
			for (Ticket ticket: this.tickets) {
				float ticketPrice = customerTicket.getPrice();
				if (ticket.equals(customerTicket) && this.wealth >= ticketPrice) {
					this.wealth -= ticketPrice;
					System.out.println("Tiket film " + customerTicket.getMovieName() + 
						" dengan waktu tayang " + customerTicket.getDay() + 
						" jenis " + customerTicket.getType() + 
						" dikembalikan ke bioskop " + this.name
						);
					return ;
				} else if (ticket.equals(customerTicket)) {
					System.out.println("Maaf ya tiket tidak bisa dibatalkan, uang kas di bioskop <Nama bioskop> lagi tekor...");
					return ;
				}
			}

			System.out.println("Maaf tiket tidak bisa dikembalikan, " + customerTicket.getMovieName() + 
				" tidak tersedia dalam " + this.name
				);
		} else {
			System.out.println("Tiket tidak bisa dikembalikan karena film " + 
				customerTicket.getMovieName() + " sudah ditonton oleh " + customerName);
			return ;
		}
		return ;
	}

	public void printInfo() {
		String margin = "------------------------------------------------------------------";
		System.out.println(margin + "\nBioskop\t\t\t: " + this.getName() +
			"\nSaldo Kas\t\t: " + this.wealth +
			"\nJumlah Tiket tersedia\t: " + this.tickets.size() +
			"\nDalam Film Tersedia\t: " + this.moviesInfo +
			"\n" + margin);
		return ;
	}

	public static void printTotalRevenueEarned(Theater[] theaters) {
		String margin = "------------------------------------------------------------------";
		String info = "";
		float wealthTotal = 0;
		
		for (Theater theater: theaters) {
			info += "\nBioskop\t\t: " + theater.getName() +
				"\nSaldo Kas\t: " + theater.getWealth() +
				"\n";
			wealthTotal += theater.getWealth();
		}

		System.out.println("Total uang yang dimiliki Koh Mas : Rp. " + wealthTotal + 
			"\n" + margin + info + margin);
		return ;
	}
}